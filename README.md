# Tutorials

### Basic fNIRS analysis
- [Basic fNIRS analysis with MNE-Python](./notebooks/mne_basic_fnirs.ipynb)

### Block average analysis
- [Bloc average analysis for OctaMon with MNE-Python](./notebooks/mne_block_average_octamon.ipynb)
- [Bloc average analysis for Brite with MNE-Python](./notebooks/mne_block_average_brite.ipynb)

### Single participant GLM analysis
- [Single participant GLM analysis for OctaMon with MNE-Python](./notebooks/mne_glm_single_octamon.ipynb)
- [Single participant GLM analysis for Brite with MNE-Python](./notebooks/mne_glm_single_brite.ipynb)

### Group level GLM analysis
- [Group level GLM analysis for OctaMon with MNE-Python](./notebooks/mne_glm_group_level_octamon.ipynb)
- [Group level GLM analysis for Brite with MNE-Python](./notebooks/mne_glm_group_level_brite.ipynb)

### NIRS Toolbox GLM analysis
- [Single participant (1st level) and group level (2nd level) and contrast (3rd level) GLM analysis (including preprocessing) for any data in NIRS format](./notebooks/nirs_toolbox_glm.m)
