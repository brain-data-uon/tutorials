{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "eight-jones",
   "metadata": {},
   "source": [
    "# Block average analysis for OctaMon with MNE-Python"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "restricted-helicopter",
   "metadata": {},
   "source": [
    "> Written by Johann Benerradi and Huimin Tang (September 2022)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "looking-celebration",
   "metadata": {},
   "source": [
    "This tutorial is based on the [official MNE tutorial](https://mne.tools/mne-nirs/stable/auto_examples/general/plot_15_waveform.html#tut-fnirs-processing).\n",
    "\n",
    "This analysis includes data loading, data processing, block averaging, data visualisation, and statistical analysis of area under the curve. This tutorial requires MNE version 1.1.1 or greater."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "olive-motion",
   "metadata": {},
   "source": [
    "## Imports and global variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "single-click",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mne\n",
    "import numpy as np\n",
    "\n",
    "from mne.channels import combine_channels\n",
    "from mne.preprocessing.nirs import source_detector_distances\n",
    "from mne.viz import plot_epochs_image, plot_compare_evokeds\n",
    "from mne.preprocessing.nirs import temporal_derivative_distribution_repair\n",
    "from pandas import DataFrame"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "facial-breathing",
   "metadata": {},
   "outputs": [],
   "source": [
    "PATHS = ['./octamon_test/p1_octamon.snirf',\n",
    "         './octamon_test/p2_octamon.snirf']\n",
    "T_MIN = -2  # in sec\n",
    "T_MAX = 25  # in sec"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "metropolitan-model",
   "metadata": {},
   "source": [
    "## Data processing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "external-refrigerator",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "raws = None  # for the concatenation of all the files' raw objects\n",
    "file_ids = []  # for events metadata\n",
    "\n",
    "for file_id, file_name in enumerate(PATHS):  # iterate over snirf files\n",
    "\n",
    "    # Load .snirf file\n",
    "    raw = mne.io.read_raw_snirf(file_name)\n",
    "    print(f'Channel names:\\n{raw.ch_names}')\n",
    "    print(f'Channel types:\\n{raw.get_channel_types()}')\n",
    "    print(f'Channel distances:\\n{source_detector_distances(raw.info)}')\n",
    "\n",
    "    # Convert sensor locations into mm\n",
    "    for ch_id in range(len(raw.info['chs'])):\n",
    "        raw.info['chs'][ch_id]['loc'][:9] *= 10\n",
    "    print(f'Fixed channel distances:\\n{source_detector_distances(raw.info)}')\n",
    "\n",
    "    # Convert light intensity to OD\n",
    "    raw = mne.preprocessing.nirs.optical_density(raw)\n",
    "\n",
    "    # TDDR\n",
    "    raw = temporal_derivative_distribution_repair(raw)\n",
    "\n",
    "    # Convert to Hb\n",
    "    raw = mne.preprocessing.nirs.beer_lambert_law(raw, ppf=6.0)\n",
    "    print(f'Channel names:\\n{raw.ch_names}')\n",
    "    print(f'Channel types:\\n{raw.get_channel_types()}')\n",
    "\n",
    "    # Bandpass filtering\n",
    "    iir_params = dict(order=4, ftype='butter', output='sos')\n",
    "    raw.filter(0.01, 0.1, method='iir', iir_params=iir_params)\n",
    "    \n",
    "    # Fix duplicate annotations/events\n",
    "    events, event_id = mne.events_from_annotations(raw)\n",
    "    events = np.unique(events, axis=0)  # keep only unique events\n",
    "    mapping = {v: k for k, v in event_id.items()}\n",
    "    annotations = mne.annotations_from_events(\n",
    "        events=events, event_desc=mapping, sfreq=raw.info['sfreq'])\n",
    "    annotations.set_durations(T_MAX)\n",
    "    raw.set_annotations(annotations)\n",
    "    \n",
    "    # Keep events file ID in metadata\n",
    "    for _ in events:\n",
    "        file_ids.append(file_id)\n",
    "\n",
    "    # Concatenate data from all files\n",
    "    if raws is None:\n",
    "        raws = raw.copy()\n",
    "    else:\n",
    "        raws.append(raw.copy())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "authentic-search",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define combinations for short channel regression\n",
    "COMBINATIONS = {\"S1_D1 hbo\": [0, 3], \"S2_D1 hbo\": [1, 3],\n",
    "                \"S3_D1 hbo\": [2, 3], \"S5_D2 hbo\": [4, 5],\n",
    "                \"S7_D2 hbo\": [6, 5], \"S8_D2 hbo\": [7, 5],\n",
    "                \"S1_D1 hbr\": [8, 11], \"S2_D1 hbr\": [9, 11],\n",
    "                \"S3_D1 hbr\": [10, 11], \"S5_D2 hbr\": [12, 13],\n",
    "                \"S7_D2 hbr\": [14, 13], \"S8_D2 hbr\": [15, 13]}\n",
    "short_sep_done = False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "opening-louis",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Short sep regression\n",
    "meas_date = raws.info['meas_date']\n",
    "annotations = raws.annotations\n",
    "annotations.set_durations(T_MAX)\n",
    "raws = combine_channels(\n",
    "    raws, groups=COMBINATIONS,\n",
    "    method=(lambda data: data[0, :] - data[1, :]))\n",
    "raws.set_meas_date(meas_date)\n",
    "raws.set_annotations(annotations)\n",
    "short_sep_done = True\n",
    "print(f'Channel names:\\n{raws.ch_names}')\n",
    "print(f'Channel types:\\n{raws.get_channel_types()}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "brazilian-bathroom",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Drop short separation channels if regression not done\n",
    "if not short_sep_done:\n",
    "    # Drop short separation channels\n",
    "    raws.drop_channels(['S4_D1 hbo', 'S4_D1 hbr', 'S6_D2 hbo', 'S6_D2 hbr'])\n",
    "    print(f'Channel names:\\n{raws.ch_names}')\n",
    "    print(f'Channel types:\\n{raws.get_channel_types()}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hundred-private",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set montage with good average spacial referencing\n",
    "montage = mne.channels.make_standard_montage('artinis-octamon')\n",
    "raws.set_montage(montage)\n",
    "sensors = raws.plot_sensors()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "certified-delaware",
   "metadata": {},
   "source": [
    "## Plot processed data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "architectural-bradley",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot concatenated data\n",
    "raws_plot = raws.plot(duration=200, scalings='auto', block=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "structural-source",
   "metadata": {},
   "source": [
    "## Plot power spectral density"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "palestinian-airplane",
   "metadata": {},
   "outputs": [],
   "source": [
    "psd_plot = raws.plot_psd(fmin=0, fmax=5, average=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "southwest-encounter",
   "metadata": {},
   "source": [
    "## Extract epochs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "knowing-wilson",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Cut epochs\n",
    "all_events, event_id = mne.events_from_annotations(raws)\n",
    "metadata = DataFrame({'File_ID': file_ids})\n",
    "epochs = mne.Epochs(raws, all_events, event_id=event_id, metadata=metadata,\n",
    "                    tmin=T_MIN, tmax=T_MAX, baseline=(None, 0), preload=True)\n",
    "epochs = epochs[['B', 'T']]\n",
    "epochs.event_id = {'Baseline': 1, 'Task': 3}\n",
    "print(epochs)\n",
    "print(f'Event IDs:\\n{epochs.event_id}')\n",
    "print(f'Events:\\n{epochs.events}')\n",
    "print(f'Events metadata: \\n{epochs.metadata}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "diagnostic-metropolitan",
   "metadata": {},
   "source": [
    "## Plot epochs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hybrid-albuquerque",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot epochs\n",
    "epochs_plot = epochs.plot(events=epochs.events, event_id=epochs.event_id,\n",
    "                          scalings='auto', n_epochs=10, block=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "standing-tokyo",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "announced-arthur",
   "metadata": {},
   "source": [
    "## Advanced visualisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "coordinated-translation",
   "metadata": {},
   "outputs": [],
   "source": [
    "conditions = epochs.event_id.keys()\n",
    "\n",
    "# Visualise with average epochs superposed for each type and each condition\n",
    "for condition in conditions:\n",
    "    print(f'*{condition}*')\n",
    "    plot_epochs_image(epochs[condition], combine='mean')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "seeing-anxiety",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualise condition comparison\n",
    "evoked_dict = {}\n",
    "styles_dict = {}\n",
    "linestyles = ['dashed', 'solid']  # for 2 conditions\n",
    "for idx, condition in enumerate(conditions):\n",
    "    evs_hbo = list(epochs[condition].pick_types(fnirs='hbo').iter_evoked())\n",
    "    evs_hbr = list(epochs[condition].pick_types(fnirs='hbr').iter_evoked())\n",
    "    for ev in evs_hbo:\n",
    "        ev.rename_channels(lambda x: x[:-4])  # remove ' hbo' from ch_names\n",
    "    for ev in evs_hbr:\n",
    "        ev.rename_channels(lambda x: x[:-4])  # remove ' hbr' from ch_names\n",
    "    evoked_dict[f'{condition}/HbO'] = evs_hbo\n",
    "    evoked_dict[f'{condition}/HbR'] = evs_hbr\n",
    "    styles_dict[condition] = {'linestyle': linestyles[idx]}\n",
    "color_dict = {'HbO': '#AA3377', 'HbR': 'b'}\n",
    "plot_compare_evokeds(evoked_dict, combine='mean', ci=0.95,\n",
    "                     colors=color_dict, styles=styles_dict,\n",
    "                     title='Conditions with 0.95 CI')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "apparent-cleaners",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Visualise conditions with time topo maps\n",
    "times = np.arange(T_MIN, T_MAX, 5)\n",
    "topomap_args = dict(extrapolate='local')\n",
    "for condition in conditions:\n",
    "    print(f'*{condition}*')\n",
    "    epochs[condition].average().plot_joint(\n",
    "        times=times, topomap_args=topomap_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "according-tuning",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "clean-state",
   "metadata": {},
   "source": [
    "## Stat analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "practical-establishment",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import extra libraries\n",
    "from scipy import stats\n",
    "from sklearn import metrics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "small-chancellor",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Combine channels based on side region of interest\n",
    "left_hbo = ['S5_D2 hbo', 'S7_D2 hbo', 'S8_D2 hbo']\n",
    "left_hbr = ['S5_D2 hbr', 'S7_D2 hbr', 'S8_D2 hbr']\n",
    "right_hbo = ['S1_D1 hbo', 'S2_D1 hbo', 'S3_D1 hbo']\n",
    "right_hbr = ['S1_D1 hbr', 'S2_D1 hbr', 'S3_D1 hbr']\n",
    "\n",
    "left_hbo_ix = mne.pick_channels(epochs.info['ch_names'], include=left_hbo)\n",
    "left_hbr_ix = mne.pick_channels(epochs.info['ch_names'], include=left_hbr)\n",
    "right_hbo_ix = mne.pick_channels(epochs.info['ch_names'], include=right_hbo)\n",
    "right_hbr_ix = mne.pick_channels(epochs.info['ch_names'], include=right_hbr)\n",
    "\n",
    "roi_dict = {\n",
    "    'left_roi_hbo': left_hbo_ix, 'left_roi_hbr': left_hbr_ix,\n",
    "    'right_roi_hbo': right_hbo_ix, 'right_roi_hbr': right_hbr_ix\n",
    "}\n",
    "\n",
    "roi_epochs = mne.channels.combine_channels(epochs, roi_dict, method='mean')\n",
    "roi_epochs.event_id = epochs.event_id\n",
    "\n",
    "# Crop pre-epoch baseline\n",
    "roi_epochs.crop(tmin=0, tmax=T_MAX)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "animated-saint",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Create table with AUC for each epoch and each ROI\n",
    "x_time = roi_epochs.times\n",
    "features = {'Conditions': [],\n",
    "            'ROI': [],\n",
    "            'AUC': [],\n",
    "            'Slope': []}\n",
    "\n",
    "for condition in roi_epochs.event_id.keys():\n",
    "    for roi in roi_epochs.ch_names:\n",
    "        for y_epoch in roi_epochs[condition].pick_channels([roi]):\n",
    "            y_epoch = y_epoch.squeeze() * 1e6  # convert M to uM\n",
    "            features['Conditions'].append(condition)\n",
    "            features['ROI'].append(roi)\n",
    "            features['AUC'].append(metrics.auc(x_time, y_epoch))\n",
    "            features['Slope'].append(stats.linregress(x_time, y_epoch).slope)\n",
    "\n",
    "df = DataFrame(features)\n",
    "print(f'AUC table:\\n{df}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "harmful-oxford",
   "metadata": {},
   "source": [
    "## Stats AUC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "initial-special",
   "metadata": {},
   "outputs": [],
   "source": [
    "CONFIDENCE = 0.05  # p-value confidence\n",
    "\n",
    "# Compute p-values\n",
    "for roi in roi_epochs.ch_names:\n",
    "    print(f'-----\\nROI: {roi}')\n",
    "    auc_conditions = []\n",
    "    test = None\n",
    "    normality = True\n",
    "    for condition in set(df['Conditions']):\n",
    "        sub_df = df[(df['ROI'] == roi) & (df['Conditions'] == condition)]\n",
    "        auc_condition = sub_df['AUC'].to_numpy()\n",
    "        auc_conditions.append(auc_condition)\n",
    "        print(f'\\t{condition} AUC average: {np.mean(auc_condition)}')\n",
    "        \n",
    "        # Check normality of the distribution\n",
    "        _, p_shap = stats.shapiro(auc_condition)\n",
    "        if p_shap < CONFIDENCE:\n",
    "            normality = False\n",
    "    if normality:\n",
    "        # t-test\n",
    "        test = 't-test'\n",
    "        _, p = stats.stats.ttest_ind(*auc_conditions)\n",
    "    else:\n",
    "        # Mann-Whitney U\n",
    "        test = \"Mann-Whitney U\"\n",
    "        _, p = stats.mannwhitneyu(*auc_conditions)\n",
    "    print(f'\\tTest:{test}\\n\\tp-value: {p}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "built-services",
   "metadata": {},
   "source": [
    "## Stats slope"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "false-hamilton",
   "metadata": {},
   "outputs": [],
   "source": [
    "CONFIDENCE = 0.05  # p-value confidence\n",
    "\n",
    "# Compute p-values\n",
    "for roi in roi_epochs.ch_names:\n",
    "    print(f'-----\\nROI: {roi}')\n",
    "    slope_conditions = []\n",
    "    test = None\n",
    "    normality = True\n",
    "    for condition in set(df['Conditions']):\n",
    "        sub_df = df[(df['ROI'] == roi) & (df['Conditions'] == condition)]\n",
    "        slope_condition = sub_df['Slope'].to_numpy()\n",
    "        slope_conditions.append(slope_condition)\n",
    "        print(f'\\t{condition} Slope average: {np.mean(slope_condition)}')\n",
    "        \n",
    "        # Check normality of the distribution\n",
    "        _, p_shap = stats.shapiro(slope_condition)\n",
    "        if p_shap < CONFIDENCE:\n",
    "            normality = False\n",
    "    if normality:\n",
    "        # t-test\n",
    "        test = 't-test'\n",
    "        _, p = stats.stats.ttest_ind(*slope_conditions)\n",
    "    else:\n",
    "        # Mann-Whitney U\n",
    "        test = \"Mann-Whitney U\"\n",
    "        _, p = stats.mannwhitneyu(*slope_conditions)\n",
    "    print(f'\\tTest:{test}\\n\\tp-value: {p}')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
