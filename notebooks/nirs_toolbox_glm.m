%% NIRS-toolbox requires the following Matlab toolboxes to work properly:
% - Matlab Statistics
% - Signal Processing

%% PREPROCESSING
% Loading data using graphical interface
root_dir = uigetdir(pwd, 'select folder'); 

% Tell the toolbox about a hierarchy in our data set, for example
% the structure has folder for a group, session and then subject on the
% lowest level
raw = nirs.io.loadDirectory((root_dir),{'group','session','subject'});
% "group" and "session" can be removed depending on the study design

%%
% plot the data for each participant
% raw(1).draw; % plot participant 1

% Visualise stimili (for example triggers)
% raw = nirs.viz.StimUtil(raw);

%%
% Rename stimuli labels
job = nirs.modules.RenameStims();
job.listOfChanges = {
     'oldA' 'newA' 
     'oldB' 'newB'
     'oldX' 'newX'};
% the label "oldA" will be changed to "newA", etc.

% Discard events that we do not need 
job = nirs.modules.DiscardStims(job);
job.listOfStims = {'oldC', 'oldD'};

% Change stimuli duration 
raw = nirs.design.change_stimulus_duration(raw,{} ,30);

% Trim baseline (5 sec before first stim onset and 5 sec after the last
% one)
job = nirs.modules.TrimBaseline(job);
job.preBaseline = 5;
job.postBaseline = 5;
job = nirs.modules.LabelShortSeperation(job); % label short separation channel
job.max_distance = 15;
job = nirs.modules.OpticalDensity(job); % convert to OD
job = nirs.modules.Resample(job); % resample to 4hz to deal with autocorrelation
job.Fs = 4; 
job = nirs.modules.TDDR(job); % TDDR fixes motion artifacts
job.usePCA = 0; % 1 to use PCA or 0 to not use it
job = nirs.modules.BeerLambertLaw(job); % convert to HB
job = nirs.modules.AddAuxRegressors(job); % label nuisance regressors, for exapmle accelometer
job.label={'aux'};
raw = job.run(raw); 
shop_raw = raw; 
save preprocessed_raw;

%% 1st LEVEL ANALYSIS
% first level analysis running subject level analysis
job1 = nirs.modules.GLM();
job1. type = 'AR-IRLS'; 
basis = nirs.design.basis.BoxCar; % can be BoxCar or Canoncal
% the choice of hemodynamic funcion depend on the study design and population
job1.AddShortSepRegressors = true;
SubjStats = job1.run(preprocessed_raw);
save SubjStats;

%% 2nd LEVEL ANALYSIS
% second level analysis running mixed models 
job2 = nirs.modules.MixedEffects();
job2.formula = 'beta ~ -1 + cond + (1|subject)';
% the formula depeends on the study design
% available Matlab documentaion on how specify the formula - https://www.mathworks.com/help/stats/fitlme.html#btyabbf
GroupStats = job2.run(SubjStats); 
GroupStats.draw('beta',[],'q<0.05');
StatsTable = GroupStats.table;

%% 3rd LEVEL CONTRAST ANALYSIS
% third level analysis testing differences between conditions
c1 = [1 -1]; % first minus second condtion

Contrasts = GroupStats.ttest(c1);
Contrasts.draw('beta', [], 'q < 0.05')
