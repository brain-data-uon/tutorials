{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "eight-jones",
   "metadata": {},
   "source": [
    "# Group level GLM analysis in Python with Brite"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "restricted-helicopter",
   "metadata": {},
   "source": [
    "> Written by Johann Benerradi (September 2022)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "written-halloween",
   "metadata": {},
   "source": [
    "This tutorial is based on the [official MNE-NIRS tutorial](https://mne.tools/mne-nirs/stable/auto_examples/general/plot_12_group_glm.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "olive-motion",
   "metadata": {},
   "source": [
    "## Imports and global variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "single-click",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import mne\n",
    "import mne_nirs\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from mne.channels import combine_channels\n",
    "from mne.preprocessing.nirs import source_detector_distances\n",
    "from mne.preprocessing.nirs import temporal_derivative_distribution_repair\n",
    "from mne_nirs.channels import get_long_channels, get_short_channels\n",
    "from mne_nirs.experimental_design import make_first_level_design_matrix\n",
    "from mne_nirs.statistics import run_glm\n",
    "from nilearn.plotting import plot_design_matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "facial-breathing",
   "metadata": {},
   "outputs": [],
   "source": [
    "PATHS = ['./brite_test/p1_brite.snirf',\n",
    "         './brite_test/p2_brite.snirf']\n",
    "T_MAX = 30  # in sec"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "metropolitan-model",
   "metadata": {},
   "source": [
    "## Group level GLM"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "external-refrigerator",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "df_ch = pd.DataFrame()  # to store channel level results\n",
    "df_roi = pd.DataFrame()  # to store region of interest results\n",
    "df_con = pd.DataFrame()  # to store channel level contrast results\n",
    "\n",
    "for file_id, file_name in enumerate(PATHS):  # iterate over snirf files\n",
    "\n",
    "    # Load .snirf file\n",
    "    raw = mne.io.read_raw_snirf(file_name)\n",
    "\n",
    "    # Convert sensor locations into mm\n",
    "    for ch_id in range(len(raw.info['chs'])):\n",
    "        raw.info['chs'][ch_id]['loc'][:9] *= 10\n",
    "\n",
    "    # Convert light intensity to OD\n",
    "    raw = mne.preprocessing.nirs.optical_density(raw)\n",
    "\n",
    "    # TDDR\n",
    "    raw = temporal_derivative_distribution_repair(raw)\n",
    "\n",
    "    # Convert to Hb\n",
    "    raw = mne.preprocessing.nirs.beer_lambert_law(raw, ppf=6.0)\n",
    "\n",
    "    # Fix duplicate annotations/events\n",
    "    events, event_id = mne.events_from_annotations(raw)\n",
    "    events = np.unique(events, axis=0)  # keep only unique events\n",
    "    mapping = {v: k for k, v in event_id.items()}\n",
    "    annotations = mne.annotations_from_events(\n",
    "        events=events, event_desc=mapping, sfreq=raw.info['sfreq'])\n",
    "    annotations.set_durations(T_MAX)\n",
    "    raw.set_annotations(annotations)\n",
    "    \n",
    "    # Delete useless annotations\n",
    "    raw.annotations.delete(raw.annotations.description == 'R')\n",
    "\n",
    "    # Downsample to reduce memory usage\n",
    "    raw.resample(1)  # resample to 1 Hz\n",
    "    \n",
    "    # Separate long and short channels\n",
    "    raw_short = get_short_channels(raw)\n",
    "    raw = get_long_channels(raw)\n",
    "\n",
    "    # Set montage with good average spacial referencing\n",
    "    montage = mne.channels.make_standard_montage('artinis-brite23')\n",
    "    raw.set_montage(montage)\n",
    "    \n",
    "    # Define design matrix\n",
    "    design_matrix = make_first_level_design_matrix(\n",
    "        raw, drift_model='cosine', high_pass=0.01, hrf_model='glover',\n",
    "        stim_dur=T_MAX)\n",
    "    \n",
    "    # Add short channels to the design matrix\n",
    "    design_matrix[\"ShortHbO\"] = np.mean(raw_short.copy().pick(\n",
    "                                        picks=\"hbo\").get_data(), axis=0)\n",
    "\n",
    "    design_matrix[\"ShortHbR\"] = np.mean(raw_short.copy().pick(\n",
    "                                        picks=\"hbr\").get_data(), axis=0)\n",
    "    \n",
    "    # Run GLM\n",
    "    glm_est = run_glm(raw, design_matrix, noise_model='auto')\n",
    "    \n",
    "    # Channel level GLM\n",
    "    ch = glm_est.to_dataframe()\n",
    "    \n",
    "    # ROI level GLM\n",
    "    left_hbo = ['S6_D5 hbo', 'S7_D4 hbo', 'S7_D5 hbo', 'S8_D5 hbo', 'S7_D6 hbo',\n",
    "                'S8_D6 hbo', 'S8_D7 hbo', 'S9_D6 hbo', 'S9_D7 hbo', 'S11_D7 hbo']\n",
    "    left_hbr = ['S6_D5 hbr', 'S7_D4 hbr', 'S7_D5 hbr', 'S8_D5 hbr', 'S7_D6 hbr',\n",
    "                'S8_D6 hbr', 'S8_D7 hbr', 'S9_D6 hbr', 'S9_D7 hbr', 'S11_D7 hbr']\n",
    "    right_hbo = ['S1_D1 hbo', 'S3_D1 hbo', 'S4_D1 hbo', 'S3_D2 hbo', 'S4_D2 hbo',\n",
    "                 'S4_D3 hbo', 'S5_D2 hbo', 'S5_D3 hbo', 'S6_D3 hbo', 'S5_D4 hbo']\n",
    "    right_hbr = ['S1_D1 hbr', 'S3_D1 hbr', 'S4_D1 hbr', 'S3_D2 hbr', 'S4_D2 hbr',\n",
    "                 'S4_D3 hbr', 'S5_D2 hbr', 'S5_D3 hbr', 'S6_D3 hbr', 'S5_D4 hbr']\n",
    "    left_hbo_ix = mne.pick_channels(raw.info['ch_names'], include=left_hbo)\n",
    "    left_hbr_ix = mne.pick_channels(raw.info['ch_names'], include=left_hbr)\n",
    "    right_hbo_ix = mne.pick_channels(raw.info['ch_names'], include=right_hbo)\n",
    "    right_hbr_ix = mne.pick_channels(raw.info['ch_names'], include=right_hbr)\n",
    "    groups = {\n",
    "        'left_roi_hbo': left_hbo_ix, 'left_roi_hbr': left_hbr_ix,\n",
    "        'right_roi_hbo': right_hbo_ix, 'right_roi_hbr': right_hbr_ix\n",
    "    }\n",
    "    conditions = ['B', 'T']\n",
    "    roi = glm_est.to_dataframe_region_of_interest(groups, conditions)\n",
    "    \n",
    "    # Contrast GLM\n",
    "    contrast_matrix = np.eye(design_matrix.shape[1])\n",
    "    basic_conts = dict([(column, contrast_matrix[i])\n",
    "                       for i, column in enumerate(design_matrix.columns)])\n",
    "    contrast_TvB = basic_conts['T'] - basic_conts['B']\n",
    "    contrast = glm_est.compute_contrast(contrast_TvB)\n",
    "    con = contrast.to_dataframe()\n",
    "    \n",
    "    # Add participant ID to the dataframes\n",
    "    roi[\"ID\"] = ch[\"ID\"] = con[\"ID\"] = file_id\n",
    "    \n",
    "    # Convert to uM for nicer plotting below.\n",
    "    ch[\"theta\"] = [t * 1.e6 for t in ch[\"theta\"]]\n",
    "    roi[\"theta\"] = [t * 1.e6 for t in roi[\"theta\"]]\n",
    "    con[\"effect\"] = [t * 1.e6 for t in con[\"effect\"]]\n",
    "    \n",
    "    # Add to the group dataframes\n",
    "    df_ch = pd.concat([df_ch, ch], ignore_index=True)\n",
    "    df_roi = pd.concat([df_roi, roi], ignore_index=True)\n",
    "    df_con = pd.concat([df_con, con], ignore_index=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "juvenile-synthetic",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_ch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "welsh-stroke",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_roi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "latin-sociology",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "df_con"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "quiet-fruit",
   "metadata": {},
   "source": [
    "## Visualise individual events"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "natural-biology",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import additional library\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "strange-practitioner",
   "metadata": {},
   "outputs": [],
   "source": [
    "grp_results = df_roi.query(\"Condition in ['B', 'T']\")\n",
    "grp_results = grp_results.query(\"Chroma in ['hbo']\")\n",
    "\n",
    "sns.catplot(x=\"Condition\", y=\"theta\", col=\"ID\", hue=\"ROI\",\n",
    "            data=grp_results, col_wrap=5, ci=None,\n",
    "            palette=\"muted\", height=4, s=10)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
